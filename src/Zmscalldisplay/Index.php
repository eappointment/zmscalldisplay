<?php
/**
 *
 * @package Zmscalldisplay
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmscalldisplay;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Slim\Request as SlimRequest;

/**
 * Handle requests concerning services
 */
class Index extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @param RequestInterface|SlimRequest $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        $validator = $request->getAttribute('validator');
        $defaultTemplate = $validator->getParameter("template")
            ->isPath()
            ->setDefault('defaultplatz')
            ->getValue();
        
        $calldisplayHelper = (new Helper\Calldisplay($request))->withResolvedCollectionsFromParameter();
        $parameters = $this->getDefaultParamters($calldisplayHelper);
        if ($request->getParam('qrcode') && $request->getParam('qrcode') == 1) {
            $parameters['showQrCode'] = true;
            $parameters['webcalldisplay'] = $this->getWebcallDisplayUrl($request);
        }
        $calldisplay = $calldisplayHelper->getEntity(true);

        $template = (new Helper\TemplateFinder($defaultTemplate))->setCustomizedTemplate($calldisplay);

        return \BO\Slim\Render::withHtml(
            $response,
            $template->getTemplate(),
            $parameters
        );
    }

    /**
     * @param Helper\Calldisplay $calldisplayHelper
     * @return array
     */
    protected function getDefaultParamters($calldisplayHelper)
    {
        $calldisplay = $calldisplayHelper->getEntity();
        $collections = [];
        if ($calldisplay->hasScopeList()) {
            $collections['scopelist'] = $calldisplay->getScopeList()->getIdsCsv();
        }
        if ($calldisplay->hasClusterList()) {
            $collections['clusterlist'] = $calldisplay->getClusterList()->getIdsCsv();
        }

        return [
            'debug' => \App::DEBUG,
            'queueStatusRequested' => implode(',', $calldisplayHelper->getRequestedQueueStatus()),
            'collections' => json_encode($collections),
            'title' => 'Aufrufanzeige',
            'calldisplay' => $calldisplay,
            'showQrCode' => false,
        ];
    }
}
