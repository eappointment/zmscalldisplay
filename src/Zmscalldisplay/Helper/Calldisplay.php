<?php
/**
 *
 * @package Zmscalldisplay
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmscalldisplay\Helper;

use BO\Mellon\Validator;

use BO\Slim\Request;
use BO\Slim\Helper\Request as RequestHelper;

use BO\Zmsentities\Calldisplay as Entity;
use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsclient\Calldisplay as CalldisplaySession;

use Psr\Http\Message\RequestInterface;

class Calldisplay
{
    /** @var Entity */
    protected $entity;
    protected $isEntityResolved = false;

    protected $request;

    const COOKIE_NAME    = 'calldisplayId';

    /** @see \BO\Zmsentities\Collection\QueueList::STATUS_CALLED **/
    const DEFAULT_STATUS = [
        ProcessEntity::STATUS_CALLED,
        ProcessEntity::STATUS_PROCESSING,
        ProcessEntity::STATUS_PICKUP,
    ];
    const WAITING_STATUS = [
        ProcessEntity::STATUS_CONFIRMED,
        ProcessEntity::STATUS_QUEUED,
        ProcessEntity::STATUS_CALLED,
        ProcessEntity::STATUS_PENDING,
    ];

    /**
     * @param Request|RequestInterface $request
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->createEntity();
    }

    /**
     * Get status for queue
     *
     * @param array|null $default (the default status array, null will return Calldisplay::DEFAULT_STATUS)
     * @return array
     */
    public function getRequestedQueueStatus(?array $default = null)
    {
        /** @var Validator $validator */
        $validator = $this->request->getAttribute('validator');
        $queue = $validator->getParameter('queue')->isArray()->getValue();
        $status = (is_array($queue) && isset($queue['status'])) ? $queue['status'] : null;
        $default = $default ?? static::DEFAULT_STATUS;

        return is_string($status) ? explode(',', $status) : $default;
    }

    /**
     * @param bool $resolveEntity
     * @return Entity
     */
    public function getEntity($resolveEntity = true)
    {
        if (!$this->isEntityResolved && $resolveEntity) {
            $this->entity = \App::$http->readPostResult('/calldisplay/', $this->entity)->getEntity();
            $this->isEntityResolved = true;
        }
        return $this->entity;
    }

    public function getSingleScope()
    {
        $scope = null;
        if (1 == $this->entity->getScopeList()->count()) {
            $scopeId = $this->entity->getScopeList()->getFirst()->getId();
            $scope = \App::$http
                ->readGetResult('/scope/'. $scopeId .'/', ['keepLessData' => ['status']])
                ->getEntity();
        }
        return $scope;
    }

    /**
     * @return Entity
     */
    protected function createEntity(): void
    {
        $calldisplay = new Entity();

        if (!CalldisplaySession::getCookieValue()) {
            $requestValue = RequestHelper::readParameterFromRequest($this->request, 'sid');
            $cookieValue  = $requestValue ?? md5((string) random_int(999999, PHP_INT_MAX));

            CalldisplaySession::setCookieValue($cookieValue, $this->request);
        } else {
            $requestValue = RequestHelper::readParameterFromRequest($this->request, 'sid');
            $cookieValue = CalldisplaySession::getCookieValue();

            if ($requestValue && $requestValue !== $cookieValue) {
                CalldisplaySession::setCookieValue($requestValue, $this->request);
                $cookieValue = $requestValue;
            }
        }

        $calldisplay->hash   = $cookieValue;
        $calldisplay->status = [
            'agent'    => RequestHelper::readParameterFromRequest($this->request, 'HTTP_USER_AGENT'),
            'template' => RequestHelper::readParameterFromRequest($this->request, 'template'),
            'qrcode'   => (int) RequestHelper::readParameterFromRequest($this->request, 'qrcode'),
            'list'     => implode(',', $this->getRequestedQueueStatus([])),
        ];
        $calldisplay->status['list'] = empty($calldisplay->status['list']) ? null : $calldisplay->status['list'];
        $this->entity = $calldisplay;
    }

    /**
     * @return $this
     */
    public function withResolvedCollectionsFromParameter(): self
    {
        /** @var Validator $validator */
        $validator = $this->request->getAttribute('validator');
        $collections = $validator->getParameter('collections')->isArray()->getValue();
        $this->entity->withResolvedCollections($collections);
        return $this;
    }

    /**
     * @return $this
     */
    public function withResolvedCollectionsFromBody(): self
    {
        /** @var Validator $validator */
        $validator = $this->request->getAttribute('validator');
        $input = $validator->getInput()->isJson()->getValue();
        if (empty($input) && !empty($this->request->getParsedBody())) {
            $input = $this->request->getParsedBody();
        }

        $collections = (isset($input['collections'])) ? $input['collections'] : [];
        if (empty($collections)) { // collections should not be empty, try fallback
            return $this->withResolvedCollectionsFromReferer();
        }
        $this->entity->withResolvedCollections($collections);
        return $this;
    }

    public function withResolvedCollectionsFromReferer(): self
    {
        if (array_key_exists('HTTP_REFERER', $this->request->getServerParams())) {
            $url = $this->request->getServerParams()['HTTP_REFERER'];
            $urlParts = parse_url($url);
            $query = $urlParts['query'] ?? [];
            if (!empty($query['collections'])) {
                $this->entity->withResolvedCollections($query['collections']);
            }
        }

        return $this;
    }
}
