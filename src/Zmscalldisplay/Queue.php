<?php
/**
 *
 * @package Zmscalldisplay
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmscalldisplay;

use BO\Slim\Render;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Queue extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        $validator = $request->getAttribute('validator');

        $calldisplayHelper = (new Helper\Calldisplay($request))->withResolvedCollectionsFromBody();
        $calldisplayEntity = $calldisplayHelper->getEntity(false);
        $queueList = \App::$http
            ->readPostResult('/calldisplay/queue/', $calldisplayEntity, [
                'statusList' => $calldisplayHelper->getRequestedQueueStatus()
            ])
            ->getCollection();

        $input  = $validator->getInput()->isJson()->getValue();
        $layout = [ // default layout, when the settings in request are missing
            'multiColumns' => 1,
            'maxResults' => 5,
            'head' => [
                'left' => 'Nummer',
                'right' => 'Platz',
            ],
        ];

        if (isset($input['tableLayout'])) {
            $layout = $input['tableLayout'];
        }

        $queueList = ($queueList) ?
            $queueList->withStatus($calldisplayHelper->getRequestedQueueStatus()) :
            new \BO\Zmsentities\Collection\QueueList();

        return Render::withHtml(
            $response,
            'block/queue/queueTable.twig',
            array(
                'tableSettings' => $validator->getParameter('tableLayout')->isArray()->setDefault($layout)->getValue(),
                'calldisplay' => $calldisplayEntity,
                'scope' => $calldisplayHelper->getSingleScope(),
                'queueList' => $queueList,
            )
        );
    }
}
