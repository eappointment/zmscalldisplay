<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmscalldisplay;

use BO\Mellon\Validator;
use BO\Slim\Headers;
use BO\Slim\Middleware\Validator as ValidatorMiddleware;
use BO\Zmsentities\ApplicationRegister;
use BO\Zmsentities\Helper\Application as ApplicationHelper;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Zmscalldisplay\Helper\Calldisplay as CalldisplayHelper;
use Slim\Psr7\Uri;

/**
 * This controller uses the Calldisplayhelper to communicate the current calldisplay instance to the ZMS-API
 */
class Ping extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @SuppressWarnings(UnusedLocalVariable)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        /** @var Validator $validator */
        $validator    = $request->getAttribute('validator');
        $url = html_entity_decode($validator->getParameter('url')->isString()->getValue() ?? '');
        if (!$url && array_key_exists('HTTP_REFERER', $request->getServerParams())) {
            $url = $request->getServerParams()['HTTP_REFERER'];
        }
        $urlParts = parse_url($url);

        $appUri       = new Uri('https', 'host', null, '/', $urlParts['query']);
        $appRequest   = $request->withUri($appUri);
        $appRequest   = ValidatorMiddleware::withValidator($appRequest);
        $calldisplay  = (new CalldisplayHelper($appRequest))->withResolvedCollectionsFromBody()->getEntity(false);
        $tmpRegEntity = new ApplicationRegister();
        $tmpRegEntity->id = $calldisplay->hash;
        $tmpRegEntity->type = ApplicationRegister::TYPE_ZMS2_CALLDISPLAY;
        $tmpRegEntity->scopeId = ApplicationHelper::getScopeIdByHash($calldisplay->hash);
        $tmpRegEntity->userAgent = $calldisplay->status['agent'] ?? null;
        $tmpRegEntity->parameters = $urlParts['query'] ?? null;

        if (!$tmpRegEntity->scopeId && $calldisplay->hasScopeList()) {
            $tmpRegEntity->scopeId = (int) $calldisplay->getScopeList()->getIds()[0];
        }

        $registerResult = \App::$http->readPostResult(
            '/applicationregister/',
            $tmpRegEntity
        );

        $pong = '';

        if ($registerResult->getResponse()->getStatusCode() <= StatusCodeInterface::STATUS_ACCEPTED) {
            $registerEntry = $registerResult->getEntity();
            /** @var ApplicationRegister $registerEntry */
            if ($registerEntry->hasChangeRequest() && isset($registerEntry->requestedChange['parameters'])) {
                $pong = json_encode([
                    'actions' => ['redirect' => '?' . $registerEntry->requestedChange['parameters']]
                ]);
            }
        }

        $response->getBody()->write($pong);

        return $response->withHeader('Content-Type', Headers::MEDIA_TYPE_APPLICATION_JSON);
    }
}
