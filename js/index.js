// --------------------------------------------------------
// ZMS Admin behavior
// --------------------------------------------------------

// Import base libs
import $ from "jquery";
import settings from './settings';

window.bo = {
    "zmscalldisplay": settings
};

// Import Views
import QueueList from "./block/queueList";
import WaitingInfo from "./block/waitingInfo";
import QrCode from "./block/qrCode";
import Ping from "./block/ping";

// Init Views
var isWaitingColumnVisible = false;
$('#queueImport').each(function() { 
    new QueueList();
});
$('.waitingInfo').each(function() {
    if (! isWaitingColumnVisible) {
        new WaitingInfo();
        isWaitingColumnVisible = true;
    }
});
new Ping();
new QrCode();

// Say hello
console.log("Welcome to the ZMS Calldisplay interface...");
