export default {
    'tableLayout' : {
        'multiColumns' : 1,
        'maxResults' : 5,
        'head' : {
            'left' : 'Nummer',
            'right' : 'Platz'
        }
    },
    'collections': {},
    'ping': {
        'interval': 60,
        'timeout': 30
    },
    'queue': {
        'calledIds' : '',
        'timeUntilOld': 60,
        'timeWaitingInfo': 60,
        'status': 'called,pickup,processing',
        'showOnlyNumeric':false
    },
    'reloadInterval': 5,
    'animationEnd': 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
    "qrCode": {
        version: 10,
        ecLevel: 'M',
        fillColor: '#000',
        backgroundColor: '#fff',
        text: 'no text',
        quiet: 5,
    }
};
