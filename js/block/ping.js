import BaseView from '../lib/baseview';
import $ from "jquery";
import { tryJson } from '../lib/utils'

class View extends BaseView {

    constructor(element) {
        super(element);
        this.bindPublicMethods('load', 'setInterval', 'ping');
        console.log('Found ping container');
        $(window).on(
            'load', () => {
                this.load();
            }
        );
    }

    load() {
        $.ajaxSetup({cache: false});
        this.setInterval();
        this.ping();
    }

    setInterval() {
        let reloadTime = window.bo.zmscalldisplay.ping.interval;
        setInterval(this.ping, reloadTime * 1000);
    }

    ping() {
        let that  = this;
        const ajaxopt = {
            type: "POST",
            url: window.bo.zmscalldisplay.includepath + '/ping/',
            data: {'url': window.location.href},
            contentType: 'application/json',
            timeout: ((window.bo.zmscalldisplay.ping.timeout * 1000) - 100)
        };
        $.ajax(ajaxopt).done(data => {
            that.processPong(data);
        })
    }

    processPong(pong) {
        let message = tryJson(pong);
        if (typeof message === 'object' && typeof message.actions === 'object') {
            for (const key in message.actions) {
                this.runAction(key, message.actions[key]);
            }
        }
    }

    runAction(type, detail) {
        if (type === 'redirect') {
            let path = window.location.href.split('?')[0];
            if (window.location.href === detail || window.location.href === path + detail) {
                return; // avoid endless redirects on error
            }
            window.location.href = detail;
        }
    }
}

export default View;