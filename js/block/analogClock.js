class ClockView {
    constructor(now) {
        this.secSyncInterval = 1000;
        this.now = now;
        this.lastSyncTime = null;
        this.clockInterval = null;

        if (this.clockInterval) {
            clearInterval(this.clockInterval);
        }
        this.clockInterval = setInterval(() => this.setClock(), this.secSyncInterval);
        this.setDate();

        document.addEventListener('visibilitychange', () => {
            if (document.visibilityState === 'visible') {
                if (this.clockInterval) {
                    clearInterval(this.clockInterval);
                }
                this.clockInterval = setInterval(() => this.setClock(), this.secSyncInterval);
                this.setDate();
            }
        });
    }

    setServerTime(now) {
        this.now = now;
        if (this.clockInterval) {
            clearInterval(this.clockInterval);
        }
        this.clockInterval = setInterval(() => this.setClock(), this.secSyncInterval);
        this.setDate();
    }

    setClock() {
        // Zeit kontinuierlich erhöhen
        this.now.setSeconds(this.now.getSeconds() + 1);

        // Zeiger der Uhr aktualisieren
        const secondHand = document.querySelector('.second-hand');
        const minsHand = document.querySelector('.min-hand');
        const hourHand = document.querySelector('.hour-hand');

        if (secondHand && minsHand && hourHand) {
            const seconds = this.now.getSeconds();
            const secondsDegrees = ((seconds / 60) * 360) + 90;
            secondHand.style.transform = `rotate(${secondsDegrees}deg)`;

            const mins = this.now.getMinutes();
            const minsDegrees = ((mins / 60) * 360) + ((seconds / 60) * 6) + 90;
            minsHand.style.transform = `rotate(${minsDegrees}deg)`;

            const hour = this.now.getHours();
            const hourDegrees = ((hour / 12) * 360) + ((mins / 60) * 30) + 90;
            hourHand.style.transform = `rotate(${hourDegrees}deg)`;
        }
    }

    setDate() {
        const dateString = document.querySelector('#aufrufanzeige_Datum');
        if (dateString) {
            const options = { weekday: 'long', year: 'numeric', month: '2-digit', day: '2-digit' };
            dateString.innerHTML = new Intl.DateTimeFormat('de-DE', options).format(this.now);
        }
    }
}

export default ClockView;
