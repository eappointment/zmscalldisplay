import $ from "jquery";
import RingAudio from "./ringAudio";
import AnalogClock from "./analogClock";

class QueueListView{

    constructor() {
        console.log('Found queueList container');
        this.queueInterval = null;
        this.syncInterval = null;
        this.analogClock = null;
        document.addEventListener('DOMContentLoaded', () => {
            this.startSynchronizedUpdate()
            this.setupIntervals();
        });
        document.addEventListener('visibilitychange', () => {
            if (document.visibilityState === 'visible') {
                this.startSynchronizedUpdate();
                this.setupIntervals();
            }
        });
    }

    setupIntervals() {
        const syncIntervalTime = 3600;
        if (this.syncInterval) {
            clearInterval(this.syncInterval);
        }
        this.syncInterval = setInterval(() => this.startSynchronizedUpdate(), syncIntervalTime * 1000);
    }

    startSynchronizedUpdate() {
        const now = new Date(window.bo.zmscalldisplay.serverTime * 1000);
        console.log("new synchronization of time");
        this.fetchQueue();
        this.setFetchIntervals();

        if (!this.analogClock) {
            this.analogClock = new AnalogClock(now);
        } else {
            this.analogClock.setServerTime(now);
        }
    }

    setFetchIntervals() {
        const reloadInterval = window.bo.zmscalldisplay.reloadInterval || 5;
        if (this.queueInterval) {
            clearInterval(this.queueInterval);
        }
        this.queueInterval = setInterval(() => {
            this.fetchQueue();
        }, reloadInterval * 1000);
    }

    fetchQueue() {
        fetch(window.bo.zmscalldisplay.includepath + '/queue/', {
            'method':'POST',
            'headers': {
                'Content-Type': 'application/json'
            },
            'body': JSON.stringify(window.bo.zmscalldisplay)
        })
            .then(response => {
                if (response.ok) {
                    $('.fatal').hide();
                    return response.text();
                }
                throw new Error(response.status + ' ' + response.statusText);
            })
            .then(html => {
                this.updateQueue(html);

            })
            .catch(error => {
                console.log(error)
                $('.fatal').show();
            });
    }

    updateQueue(html) {
        $('#queueImport').html(html);
        let audioCheck = new RingAudio();
        audioCheck.initSoundCheck();
        this.getDestinationToNumber();
    }

    getUrl(relativePath) {
        let includepath = window.bo.zmscalldisplay.includepath;
        return includepath + relativePath;
    }

    getDestinationToNumber() {
        if (window.bo.zmscalldisplay.queue.showOnlyNumeric) {
            $('#queueImport .destination').each(function () {
                let string = $(this).text();
                let regex = /\d/g;
                if (regex.test(string)) {
                    $(this).text(string.replace(/\D/g, ''));
                }
            });
        }
    }
}

export default QueueListView;
