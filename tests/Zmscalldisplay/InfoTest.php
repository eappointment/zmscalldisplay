<?php

declare(strict_types=1);

namespace BO\Zmscalldisplay\Tests;

use Fig\Http\Message\StatusCodeInterface;

class InfoTest extends Base
{
    protected $classname = "Info";

    public function testRendering()
    {
        $this->setApiCalls([
            'GET_queue' => [
                'function' => 'readPostResult',
                'url' => '/calldisplay/queue/',
                'response' => $this->readFixture("GET_queue.json")
            ]
        ]);

        $response = $this->render([ ], [
            'collections' => [
                'scopelist' => '141,142',
                'clusterlist' => '110'
            ],
        ], [ ]);

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
        self::assertStringContainsString('waitingClients', (string) $response->getBody());
        self::assertStringContainsString('waitingTime', (string) $response->getBody());
    }

    public function testRenderingWithWaitingCitizen(): void
    {
        $this->setApiCalls([
            'GET_queue' => [
                'function' => 'readPostResult',
                'url' => '/calldisplay/queue/',
                'response' => $this->getQueueWithWaitingTime()
            ]
        ]);

        $response = $this->render([ ], [
            'collections' => [
                'scopelist' => '141,142',
                'clusterlist' => '110'
            ],
        ], [ ]);

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
        self::assertStringContainsString('0 - 45 Minuten', (string) $response->getBody());
    }

    protected function getQueueWithWaitingTime(): string
    {
        return '
        {
          "$schema": "https://localhost/terminvereinbarung/api/2/",
          "meta": {
            "$schema": "https://schema.berlin.de/queuemanagement/metaresult.json",
            "error": false,
            "generated": "2016-04-29T11:53:31+02:00",
            "server": "Zmsapi-unconfigured"
          },
          "data": {
            "0": {
                "$schema": "https:\/\/schema.berlin.de\/queuemanagement\/queue.json",
                "arrivalTime": 1459490400,
                "callCount": "0",
                "callTime": 0,
                "number": "150897",
                "waitingTime": 0,
                "destination": "Ausgabe",
                "withAppointment": true,
                "status": "",
                "waitingTimeEstimate": 30
            },
            "1": {
                "$schema": "https:\/\/schema.berlin.de\/queuemanagement\/queue.json",
                "arrivalTime": 1459490400,
                "callCount": "0",
                "callTime": 0,
                "number": "88478",
                "waitingTime": 15,
                "destination": "Ausgabe",
                "withAppointment": true,
                "status": "",
                "waitingTimeEstimate": 45
            }
          }
        }';
    }
}
