<?php

namespace BO\Zmscalldisplay\Tests;

use BO\Zmsentities\Process as ProcessEntity;

class QueueDestinationTest extends Base
{
    protected $classname = "Queue";

    protected $arguments = [ ];

    protected $parameters = [ ];

    protected function getApiCalls()
    {
        return [
            [
                'function' => 'readPostResult',
                'url' => '/calldisplay/queue/',
                'parameters' => [
                    'statusList' => [
                        ProcessEntity::STATUS_CALLED,
                        ProcessEntity::STATUS_PROCESSING,
                        ProcessEntity::STATUS_PICKUP,
                    ]
                ],
                'response' => $this->readFixture("GET_queue_multipleDestination.json")
            ]
        ];
    }

    public function testRendering()
    {
        $response = $this->render([ ], [
            '__body' => '{
                "collections": {"scopelist":"141,140"},
                "tableLayout": {
                    "multiColumns": 2,
                    "maxResults": 8,
                    "head":{
                        "left": "Nummer",
                        "right": "Platz"
                    }
                }
            }'
        ], [ ]);
        $this->assertStringContainsString('31316', (string) $response->getBody());
        $this->assertStringContainsString('52230', (string) $response->getBody());
        $this->assertStringContainsString('data="10"', (string) $response->getBody());
        $this->assertStringContainsString('data="12"', (string) $response->getBody());
    }
}
