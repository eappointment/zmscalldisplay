<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmscalldisplay\Tests;

use Fig\Http\Message\StatusCodeInterface;

class PingTest extends Base
{
    protected $classname = "Ping";
    protected $arguments = [];
    protected $parameters = [];

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readPostResult',
                    'url' => '/applicationregister/',
                    'response' => $this->readFixture("POST_applicationregister_calldisplay_twoScopes.json"),
                ],
            ]
        );
        $queryStr = 'collections[scopelist]=140,141&queue[status]=called,pickup,processing&template=defaultplatz';
        $response = $this->render([ ], [
            'url' => 'https://localhost/calldisplay/?' . $queryStr,
        ], [ ]);

        self::assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
        self::assertStringContainsString('redirect', (string) $response->getBody());
    }
}
